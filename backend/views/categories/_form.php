<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\categories\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-footer">
    <div class="pull-right">
        <button class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
        <button class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
    </div>
    <button class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
    <button class="btn btn-default"><i class="fa fa-print"></i> Print</button>
</div>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'img')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('categories', 'Create') : Yii::t('categories', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<div class="box-footer">
    <div class="pull-right">
        <button class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
        <button class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
    </div>
    <button class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
    <button class="btn btn-default"><i class="fa fa-print"></i> Print</button>
</div>
