<?php

namespace common\models\articles;

use yii\behaviors\SluggableBehavior;

use common\models\categories\Category;
use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%articles}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 * @property string $alias
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $js
 * @property string $css
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * @property Category $category
 * @property User $user
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%articles}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'category_id'], 'required'],
            [['user_id', 'category_id', 'status'], 'integer'],
            [['description', 'content', 'js', 'css'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['alias', 'title'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('articles', 'ID'),
            'user_id'     => Yii::t('articles', 'User ID'),
            'category_id' => Yii::t('articles', 'Category ID'),
            'alias'       => Yii::t('articles', 'Alias'),
            'title'       => Yii::t('articles', 'Title'),
            'description' => Yii::t('articles', 'Description'),
            'content'     => Yii::t('articles', 'Content'),
            'js'          => Yii::t('articles', 'JS'),
            'css'         => Yii::t('articles', 'CSS'),
            'status'      => Yii::t('articles', 'Status'),
            'created_at'  => Yii::t('articles', 'Created At'),
            'updated_at'  => Yii::t('articles', 'Updated At'),
            'deleted_at'  => Yii::t('articles', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }
}
