<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\articles\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-footer">
    <div class="pull-right">
        <button class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
        <button class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
    </div>
    <button class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
    <button class="btn btn-default"><i class="fa fa-print"></i> Print</button>
</div>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList([1,2,4], ['prompt' => Yii::t('app', 'Select status...')]) ?>

    <?= $form->field($model, 'css')->widget(trntv\aceeditor\AceEditor::className(), ['mode' => 'css', 'theme' => 'sqlserver']) ?>

    <?= $form->field($model, 'js')->widget(trntv\aceeditor\AceEditor::className(), ['mode' => 'javascript', 'theme' => 'sqlserver']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('articles', 'Create') : Yii::t('articles', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<div class="box-footer">
    <div class="pull-right">
        <button class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
        <button class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
    </div>
    <button class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
    <button class="btn btn-default"><i class="fa fa-print"></i> Print</button>
</div>
