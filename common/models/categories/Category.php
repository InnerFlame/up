<?php

namespace common\models\categories;

use creocoder\nestedsets\NestedSetsBehavior;
use yii\behaviors\SluggableBehavior;

use common\models\articles\Article;
use Yii;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $alias
 * @property string $description
 * @property integer $status
 * @property string $img
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 *
 * @property Article[] $articles
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status', 'lft', 'rgt', 'depth', 'root', 'parent_id'], 'integer'],
            [['title', 'description'], 'string'],
            [['title', 'alias', 'img'], 'string', 'max' => 255],
            [['depth', 'root', 'rgt', 'lft', 'parent_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'slug' => [
                'class'         => SluggableBehavior::className(),
                'attribute'     => 'title',
                'slugAttribute' => 'alias',
                'immutable'     => 'true',
            ],
            'tree' => [
                'class'          => NestedSetsBehavior::className(),
                'treeAttribute'  => 'root',
                'leftAttribute'  => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'depth',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('categories', 'ID'),
            'parent_id'   => Yii::t('categories', 'Parent Id'),
            'title'       => Yii::t('categories', 'Title'),
            'alias'       => Yii::t('categories', 'Alias'),
            'description' => Yii::t('categories', 'Description'),
            'status'      => Yii::t('categories', 'Status'),
            'root'        => Yii::t('categories', 'Root'),
            'img'         => Yii::t('categories', 'Img'),
            'lft'         => Yii::t('categories', 'Lft'),
            'rgt'         => Yii::t('categories', 'Rgt'),
            'depth'       => Yii::t('categories', 'Depth'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id']);
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }
}
