<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // lte
        'theme/lte/bootstrap/css/bootstrap.min.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        'http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css',
        'theme/lte/dist/css/AdminLTE.min.css',
        'theme/lte/dist/css/skins/_all-skins.min.css',
        'theme/lte/plugins/iCheck/flat/blue.css',
        'theme/lte/plugins/morris/morris.css',
        'theme/lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'theme/lte/plugins/datepicker/datepicker3.css',
        'theme/lte/plugins/daterangepicker/daterangepicker-bs3.css',
        'theme/lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        // lte end
        'css/site.css',
    ];
    public $js = [
        // ie <9
        'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js',
        'https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js',
        // ie <9 end
        '/theme/lte/plugins/jQuery/jQuery-2.1.3.min.js',
        'http://code.jquery.com/ui/1.11.2/jquery-ui.min.js',
        '/theme/lte/bootstrap/js/bootstrap.min.js',
        'http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
//        '/theme/lte/plugins/morris/morris.min.js',
        '/theme/lte/plugins/sparkline/jquery.sparkline.min.js',
        '/theme/lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        '/theme/lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        '/theme/lte/plugins/knob/jquery.knob.js',
        '/theme/lte/plugins/daterangepicker/daterangepicker.js',
        '/theme/lte/plugins/datepicker/bootstrap-datepicker.js',
        '/theme/lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        '/theme/lte/plugins/iCheck/icheck.min.js',
        '/theme/lte/plugins/slimScroll/jquery.slimscroll.min.js',
        '/theme/lte/plugins/fastclick/fastclick.min.js',
        '/theme/lte/dist/js/app.min.js',
//        '/theme/lte/dist/js/pages/dashboard.js',
        '/theme/lte/dist/js/demo.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
