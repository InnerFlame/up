<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m171004_174344_initial extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%categories}}', [
            'id'          => $this->primaryKey(),
            'parent_id'          => $this->integer(),
            'title'       => $this->string()->notNull(),
            'alias'       => $this->string(),
            'description' => $this->text(),
            'status'      => $this->integer(),
            'img'         => $this->string(),
            'root'        => $this->integer(),
            'lft'         => $this->integer(),
            'rgt'         => $this->integer(),
            'depth'       => $this->integer(),
        ]);

        $this->createTable('{{%articles}}', [
            'id'          => $this->primaryKey(),
            'user_id'     => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'alias'       => $this->string(),
            'title'       => $this->string(),
            'description' => $this->text(),
            'content'     => $this->text(),
            'js'          => $this->text(),
            'css'         => $this->text(),
            'status'      => $this->integer(),
            'created_at'  => $this->timestamp(),
            'updated_at'  => $this->timestamp(),
            'deleted_at'  => $this->timestamp(),
        ]);

        $this->createIndex('idx-articles-user_id', '{{%articles}}', 'user_id');
        $this->addForeignKey('fk-articles-user_id', '{{%articles}}', 'user_id', '{{%user}}', 'id', 'CASCADE');

        $this->createIndex('idx-categories-category_id', '{{%articles}}', 'category_id');
        $this->addForeignKey('fk-categories-category_id', '{{%articles}}', 'category_id', '{{%categories}}', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-articles-user_id', '{{%articles}}');
        $this->dropIndex('idx-articles-user_id', '{{%articles}}');
        $this->dropForeignKey('fk-categories-category_id', '{{%articles}}');
        $this->dropIndex('idx-categories-category_id', '{{%articles}}');

        $this->dropTable('{{%articles}}');
        $this->dropTable('{{%categories}}');
    }
}
